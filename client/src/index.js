import '../dist/css/style.css';

import React from 'react';
import ReactDOM from 'react-dom';
import {Route, BrowserRouter, Switch} from 'react-router-dom';
import App from './components/App';
import Chapters from './components/pages/navigation/Chapters';
import ChapterDashboard from './components/pages/dashboards/ChapterDashboard';
import Schools from './components/pages/navigation/Schools';
import SchoolDashboard from './components/pages/dashboards/SchoolDashboard'
import RallyDashboard from './components/pages/dashboards/RallyDashboard';
import WitnessingDashboard from './components/pages/dashboards/WitnessingDashboard';
import WitnessingEvents from './components/pages/navigation/WitnessingEvents';
import Rallies from './components/pages/navigation/Rallies';
import MainComponent from './components/pages/dashboards/MainComponent';

ReactDOM.render(
    <BrowserRouter forceRefresh={true}>
		<App>
            <Switch>
                    <Route path = "/witnessing" component={WitnessingEvents} />
                    <Route strict path="/chapters" component={Chapters} />
                    <Route path="/chapter/:grouping_title?/:min_date?/:max_date?" component={ChapterDashboard} />
                    <Route strict path="/schools" component={Schools} />
                    <Route path="/school/:id?/:min_date?/:max_date?" component={SchoolDashboard} />
                    <Route path="/rallies" component={Rallies} />
                    <Route strict path="/rally/:id?" component={RallyDashboard} />
                    <Route strict path = "/witnessingday/:id?" component={WitnessingDashboard} />
                    <Route path="/" component={MainComponent} />
            </Switch>
		</App>
	</BrowserRouter>,
    document.getElementById('content')
);
