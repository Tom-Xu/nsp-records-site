import {
	withDBConnection,
	isLoggedIn,
} from '../util';
import {
	requestContactInfo,
	suggestEdit,
} from './MailController';

export default app => {
	app.route('/api/contactinfo/:rallyID')
		.get(isLoggedIn, withDBConnection(requestContactInfo));

	app.route('/api/suggestedit/:type/:id/:edit')
		.get(isLoggedIn, suggestEdit);
}
