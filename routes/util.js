import * as STATUS_CODES from 'http-status-codes';
import mysql from 'mysql';
import databaseCredentials from './database-credentials.json';
const siteUrl = process.env.MODE !== 'production' ? `http://records.localhost:${process.env.RECORDS_PORT || 8080}` : "http://records.nationalschoolproject.com";


//Initialize our database connections
const pool = mysql.createPool(databaseCredentials.url);
const adminPool = mysql.createPool(databaseCredentials.adminUrl);

const studentDevelopers = [
	"mateo.d.langstonsmith@biola.edu",
	"tjyurek@gmail.com",
	"aaron.b.chan@biola.edu",
	"enle.xu@biola.edu",
	"noa.d.wilding@biola.edu",
	"kailynne.l.lopez@biola.edu",
	"mia.m.honcoop@biola.edu",
	"joseph.william.stewart@biola.edu"
];

export const getConnectionPool = () => pool;
export const getSiteUrl = () => siteUrl;
export const getHomeDirectory = () => __dirname + `/../${process.env.NSP_DIR || 'nsp-metrics'}`;

/***
 * @description Takes a users email and determines if that user is a student developer
 */
export const isStudentDeveloper = email => studentDevelopers.includes(email);

/***
 * @description Takes an array and formats it to be used in a SQL query
 ***/
export const formatArrayForSQL = a => {
	if (a.length == 0) {
		a.push(-1);
	}
	return JSON.stringify(a).replace("[","(").replace("]",")");
}

/***
 * @description Wraps a callback function so that it can utilize a database connection
 ***/
export const withDBConnection = callback => {
	return (request, response) => {
		try {
			pool.getConnection(
				(error, connection) => {
					if (error) {
						return response.send(error);
					}
					else {
						return callback(request, response, connection);
					}
				}
			);
		}
		catch (e) {
			response.error(e.stringify());
		}
	}
}

/***
 * @description Wraps a callback function so that it can utilize an admin database connection
 ***/
export const withAdminDBConnection = callback => {
	return (request, response) => {
		try {
			adminPool.getConnection(
				(error, connection) => {
					if (error) {
						return response.send(error);
					}
					else {
						return callback(request, response, connection);
					}
				}
			);
		}
		catch (e) {
			response.error(e.stringify());
		}
	}
}

/***
 * @description Checks if user is logged in
 ***/
export const isLoggedIn = (request,response,next) => {
	if (request.isAuthenticated() || process.env.NO_AUTH) {
		return next();
	}

	response.redirect(siteUrl + '/login');
	response.status(200).end();
}

/***
 * @description Returns a JSON obeject describing which schools and chapters the user has access to
 ***/
export const getPermissions = request => {

	if (process.env.NO_AUTH) {
		return {
			chaptersSQL: '(-1)',
			schoolsSQL: '(-1)',
			chapters: [],
			schools: [],
			hasFullAccess: true,
			isAdmin: true,
		}
	}

	let chapters = request.user.chapters;
	let schools = request.user.schools;
	let hasFullAccess = request.user.hasFullAccess;
	let isAdmin = request.user.isAdmin;

	if (!hasFullAccess)
		hasFullAccess = false;

	if (chapters == undefined)
		chapters = [];

	if (schools == undefined)
		schools = [];

	return {
		chaptersSQL: formatArrayForSQL(chapters),
		schoolsSQL: formatArrayForSQL(schools),
		chapters,
		schools,
		hasFullAccess,
		isAdmin,
	};
}

/***
 * @description Handles return a callback function to be used by a SQL query
 ***/
export const returnSQLResponse = (connection, response, transformResults) => {
	return (error, results) => {
		connection.release();
		if (error) {
			console.log(error);
			response.status(STATUS_CODES.INTERNAL_SERVER_ERROR).json(JSON.stringify(error));
		} else {
			response.json(transformResults ? transformResults(results) : results);
		}
	}
}

/***
 * @description Transforms a change set (i.e. {col_to_change: new_value}) into an SQL update string
 ***/
export const getUpdateStatementFromChangeSet = changes => {
	let rval = "";
	let SQLChanges = [];

	for (let key in changes) {
		const obj = {};
		obj.key = key;
		obj.val = changes[key];
		SQLChanges.push(obj);
	}

	for (let i = 0; i < SQLChanges.length; i++) {
		rval += SQLChanges[i].key.replace(/'/g, "\\'") + " = '" + (typeof SQLChanges[i].val == "string" ? SQLChanges[i].val.replace(/'/g, "\\'") : SQLChanges[i].val) + "'"
		if (i != SQLChanges.length - 1) {
			rval += ", "
		}
	}

	return rval;
}
