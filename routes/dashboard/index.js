import {
	withDBConnection,
	isLoggedIn
} from '../util';
import {
	getWitnessingDaysForYear,
	getRalliesForYear,
	getSchoolsForYear,
	getRallyStories,
	getRallyPictures,
	getWitnessingPictures,
	getWitnessingStories,
	getDashBoard
} from './DashboardController';

export default app => {
	app.route('/api/dashboard/:start_date/:end_date')
		.get(isLoggedIn, withDBConnection(getDashBoard))

	app.route('/api/dashboard/witnessingstories')
		.get(isLoggedIn, withDBConnection(getWitnessingStories))

	app.route('/api/dashboard/witnessingpictures')
		.get(isLoggedIn, withDBConnection(getWitnessingPictures))

	app.route('/api/dashboard/rallypictures')
		.get(isLoggedIn, withDBConnection(getRallyPictures))

	app.route('/api/dashboard/rallystories')
		.get(isLoggedIn, withDBConnection(getRallyStories))
}