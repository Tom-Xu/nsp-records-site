/**
 * @typedef {Object} ParsedQuery
 * @property {string} entity The table to select from
 * @property {string} [field] The table column to select
 * @property {string} [qualifier] The where clause of the SQL query
 * @property {string} [groupByEntity] The entity to group analytics by
 * @property {string} [groupByEntityQualifier] A boolean expression to filter entity groups
 * @property {string} [err] A flag that is set if there is an error in the parsing
 */

/**
 * @typedef {Object} QualifierExpression
 * @property {string} [result] The transpiled expression
 * @property {string} [err] A flag that is set if an error occurs
 * @property {string} [reason] Specific reason why an error was thrown
 */

import { fieldAnomalies } from './SQLBuilder';

const parser = /([\-0-9A-Z_a-z]+)\s*(\(\s*([\-0-9A-Z_a-z]+)\s*\))?\s*({\s*([\-0-9A-Z_a-z\s=><!"'\(\),)]+)\s*})?\s*((FOREACH|FOR EACH)\s+([\-0-9A-Z_a-z]+)\s*({\s*([\-0-9A-Z_a-z\s=><!"']+)\s*})?)?/gi
const boolExpressionParser = /\s*(([\-0-9A-Z_a-z]+)\s*(!=|<=|>=|>|<|=|IN)\s*(".+"|'.+'|false|true|[0-9]+|\(.+\)))\s*(AND|OR)?/gi;

const schoolFields = [
	'name',
	'id',
	'state',
	'county',
	'address',
	'zip',
	'students_at_school', // Takes average for all enrollments of school
	'enrollments',
	'year',
];

const chapterFields = [
	'name',
	'state',
	'id',
	'status',
];

const rallyFields = [
	'card_explained',
	'club_attendance',
	'was_contact_cards', // Treats column as boolean where Yes=true (case insensitve)
	'date',
	'indicated_decisions',
	'materials', // Takes sum of all materials, only counts rows where at least of the material columns is non-zero
	'materials_bible', // Only counts rows where materials_bible is non-zero
	'materials_gospels_of_john',
	'materials_lifebooks',
	'materials_other_type',
	'was_materials_table', // Y/N bool
	'was_performer', // Y/N bool
	'was_speaker', // Y/N bool
	'performer_effectiveness',
	'speaker_engagement',
	'speaker_explained_saved', // Y/N bool
	'speaker_gospel_clear',
	'speaker_gospel_shared', // Y/N bool
	'speaker_physical_invitation', // Y/N bool
	'speaker_student', // Boolean where Student=true (case insensitive)
	'speaker_guest', // Boolean where Guest=true (case insensitive)
	'speaker_topically_known', // Y/N bool
	'students_participating',
	'total_attendance',
	'speaker_explained_saved_clear',
	'chapter_id',
	'school_id',
];

const witnessingFields = [
	'approached',
	'bibles',
	'conversations_with_christians',
	'csp_members',
	'date',
	'follow_up_requests',
	'gospel_booklets',
	'gospel_presentations',
	'gospels_of_john',
	'hs_student_witness',
	'indicated_decisions',
	'lifebooks',
	'was_location_hs', // Y/N bool
	'recommittments',
	'satisfied',
	'spirit_filled_life',
	'chapter_id',
	'school_id',
];

const fields = {
	rallies: rallyFields,
	schools: schoolFields,
	witnessing_day: witnessingFields,
	chapters: chapterFields,
};

const aliases = {
	fields: {
		witnessing: 'witnessing_day',
		chapter: 'chapters',
		rally: 'rallies',
		school: 'schools',
	},
	witnessing_day: {
		presentations: 'gospel_presentations',
		nsp_members: 'csp_members',
		location_hs: 'was_location_hs',
		chapter: 'chapter_id',
		school: 'school_id',
	},
	rallies: {
		gospels_of_john: 'materials_gospels_of_john',
		bibles: 'materials_bibles',
		lifebooks: 'materials_lifebooks',
		gospel_shared: 'speaker_gospel_shared',
		chapter: 'chapter_id',
		school: 'school_id',
	},
	schools: {
		population: 'students_at_school',
		total_population: 'students_at_school',
	}
};

const validGroupByCombinations = {
	schools: [
		'chapters',
		'year',
	],
	chapters: [],
	rallies: [
		'schools',
		'chapters',
		'year',
	],
	witnessing_day: [
		'schools',
		'chapters',
		'year',
	]
}

/**
 * @description Checks a entity or field name against an alias map and returns the proper name to use
 * @param {Object} aliasMap The alias map to check against
 * @param {string} str The string to be resolved
 * @returns {string} Either the alias found in aliasMap or str
 */
const resolveAlias = (aliasMap, str) => (aliasMap && aliasMap[str]) || str;

/**
 * @description Checks if a given string is a valid field for a given entity
 * @param {string} entity The entity to check for
 * @param {string} field The field to validate
 * @returns {boolean} Whether or not field is valid for entity
 */
const isValidField = (entity, field) => fields[entity] && fields[entity].includes(field);

/**
 * @description Checks if a given groupByEntity can be paried with the given entity
 * @param {string} entity The main entity to select from
 * @param {string} groupByEntity The entity we are trying to group by
 * @returns {boolean} Whether or not entity and groupByEntity form a valid combination
 */
const isValidGroupByEntity = (entity, groupByEntity) => validGroupByCombinations[entity].includes(groupByEntity);

const resolveSelectFieldAnomaly = field => {
	if (fieldAnomalies[field] && fieldAnomalies[field].SELECT)
		return fieldAnomalies[field].SELECT;
	return field;
}

const resolveWhereFieldAnomaly = field => {
	if (fieldAnomalies[field] && fieldAnomalies[field].WHERE)
		return `AND ${fieldAnomalies[field].WHERE}`
	return '';
}

/**
 * @description Breaks apart a boolean expression that qualifies some entity selection, validates it, and returns a string that can be inserted into a SQL WHERE clause
 * @param {string} entity The entity we are qualifying. This is the entity that fields in the qualifier statement will be checked against
 * @param {string} statement The statement we are validating and transpiling
 * @returns {QualifierExpression} The transpiled statement in object 
 */
const deconstructQualifier = (entity, statement) => {
	// Return nothing if statement is undefined
	if (!statement || !statement.trim()) {
		return {result: undefined}
	}

	/**
	 * Each time we iterate through the statement, we are given a fragment of the 
	 * string representing a boolean expression. If we concatentate all of those
	 * fragments, we should have the original string. If we don't then we know that
	 * one part of the statement did not match the boolean expression pattern.
	 */
	let totalStatement = '';
	let expectingNextExpression = false;
	let err;
	let reason;

	// Iterate through statement one atomic boolean expression at a time
	let result = statement.replace(boolExpressionParser, (statementFragment, ...match) => {
		const field = resolveAlias(aliases[entity], match[1]);
		const op = match[2];
		const operand = match[3];
		const connector = match[4];

		// Validate left hand side of boolean expression as a field for entity
		if (!isValidField(entity, field)) {
			err = 'InvalidField';
			reason = `${field} is not a valid field for ${entity}`
		}
		expectingNextExpression = !!match[4];
		totalStatement += statementFragment;
		return `${resolveSelectFieldAnomaly(`${entity}.${field}`)} ${op} ${operand}${connector ? ` ${connector}` : ''} ${resolveWhereFieldAnomaly(`${entity}.${field}`)}`;
	});

	if (totalStatement.trim() !== statement.trim()) {
		// Throw error if part of the statement does not match the pattern
		return {err: 'BadExpression', reason: `"${statement}" != "${totalStatement}"`};
	} else if (expectingNextExpression) {
		// Throw error if statement ends in AND or OR
		return {err: 'BadExpression', reason: `Expecting next expression in ${statement}`}
	} else if (err) {
		return {err, reason};
	} else {
		return {result};
	}
}

/**
 * @description Breaks apart an analytics query into different parts to be inserted into an SQL query
 * @param query The query to be parsed in the form of <entity> (<field>) {<qualifier>} FOR EACH <entity> {<qualifier>}
 * @returns {ParsedQuery} The parsed query object, or an object with the field err if an error was thrown
 */
export default query => {
	parser.lastIndex = 0;
	boolExpressionParser.lastIndex = 0;

	if (typeof query !== 'string') {
		// Return error if query is not a string
		return {err: 'BadQueryType'};
	} else if (!query) {
		// Return error if query is falsey
		return {err: 'NoQueryGiven'};
	}

	const match = parser.exec(query);
	if (!match) {
		return {err: 'BadQuery', reason: `${query} did not match query pattern`};
	}

	const parsedExpression = match[0];

	// The entire expression should be matched by the pattern, if not return an error
	if (parsedExpression.trim() !== query.trim()) {
		return {err: 'BadQuery', reason: `"${parsedExpression.trim()}" != "${query.trim()}"`};
	}

	// Extract and validate entity name
	const entity = resolveAlias(aliases.fields, match[1]);
	if (entity && entity.trim() && !fields[entity]) {
		return {err: 'CouldNotResolveEntity', reason: `${entity} could not be resolved`};
	} else if (!entity || !entity.trim()) {
		return {err: 'NoEntityGiven', reason: ''};
	}

	// Extract and validate field to select on if one is given
	const hasFieldSpecification = !!match[2];
	const field = resolveAlias(aliases[entity], match[3]);
	if (hasFieldSpecification && !isValidField(entity, field)) {
		return {err: 'InvalidField', reason: `${field} is an invalid field for ${entity}`};
	}

	// Extract and validate qualifier statement for first entity if one exists
	const qualifier = deconstructQualifier(entity, match[5]);
	if (qualifier.err) {
		return qualifier;
	}

	// Extract and validate entity to group results by if one exists
	const groupByEntity = resolveAlias(aliases.fields, match[8]);
	if (groupByEntity && !isValidGroupByEntity(entity, groupByEntity)) {
		return {err: 'InvalidGroupByCombination', reason: `${groupByEntity} is not a valid group by entity for ${entity}`};
	}

	// Extract and validate qualifier statement for second entity if one exists
	const groupByEntityQualifier = deconstructQualifier(groupByEntity, match[10]);
	if (groupByEntityQualifier.err) {
		return groupByEntityQualifier;
	}

	return {
		entity,
		field: field ? `${entity}.${field}` : undefined,
		qualifier: qualifier ? qualifier.result : undefined,
		groupByEntity,
		groupByEntityQualifier: groupByEntityQualifier ? groupByEntityQualifier.result : undefined,
	}
}
