/*
const cont = {continued:true};
const title = {underline:true,align:"center"};
const indent1Cont = {indent:36,continued:true};
const indent2Cont = {indent:72,continued:true};
*/

export default {
	yellow: "#EEA00F",
	blue: "#1A2D6B",
	black: "black",
	cont: { continued: true },
	title: { underline: true, align: "center" },
	indent1: { indent: 36, continued: true },
	indent2: { indent: 72, continued: true }
}
